HQ Weapon Icons +Colored
by Camil1999

ENG:
This mod replace the old weapon icons with high quality ones.
Includes grey and colored version.

Installation:

ORIGINAL WEAPONS
1. If you want white icons:
Replace the files from "gta3.img" folder in gta3.img archive.
2. If you want colored version:
Do the same, but with the files from "gta3.img - colored" instead.

MODDED WEAPONS
1. If you want white icons:
Export the .txd files of weapons from gta3.img archive and replace the texture images (included in "preview" folder) with TXD Workshop.
2. If you want colored icons:
Do same, but this time replace with images from "preview colored"
----------------------------------------------------------------------------
PL:
Mod dodaje ikony broni w jako�ci HD.
Zawiera dwie wersje: szare oraz kolorowe ikonki.

Instalacja:

ORYGINALNE BRONIE
1. Bia�e ikony:
Pliki z folderu "gta3.img" podmie� w archiwum gta3.img
2. Kolorowe ikony:
Zr�b to samo, lecz zamiast plik�w z wy�ej wymienionego folderu, podmie� pliki z "gta3.img - colored".

NIESTANDARDOWE BRONIE
1. Bia�e ikony:
Wypakuj pliki .txd broni (ak47.txd,bat.txd itd.) i za pomoc� TXD Workshop zamie� tekstury, kt�re zawarte s� w folderze "preview"
2. Kolorowe ikony:
Zr�b podobnie, ale zamiast tekstur z "preview" zamie� na tekstury z folderu "preview colored"