// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once 
#include "GameFramework/HUD.h"
#include "spikePe4HUD.generated.h"

UCLASS()
class AspikePe4HUD : public AHUD
{
	GENERATED_BODY()

public:
	AspikePe4HUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

