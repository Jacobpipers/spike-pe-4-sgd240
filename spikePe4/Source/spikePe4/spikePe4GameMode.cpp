// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "spikePe4.h"
#include "spikePe4GameMode.h"
#include "spikePe4HUD.h"
#include "spikePe4Character.h"

AspikePe4GameMode::AspikePe4GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AspikePe4HUD::StaticClass();
}
