// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "spikePe4GameMode.generated.h"

UCLASS(minimalapi)
class AspikePe4GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AspikePe4GameMode();
};



