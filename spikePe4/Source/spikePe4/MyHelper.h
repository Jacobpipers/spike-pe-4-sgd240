// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <Engine.h>

#define SDEBUG(V) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, V);
