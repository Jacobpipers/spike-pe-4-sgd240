# Spike Report

## SPIKE PE 4 - Destruction and Damage

### Introduction

We can interact with things now – but we also want to be able to break them!

### Goals

* The Spike Report should answer each of the Gap questions

In a new “demo” project, which utilises elements from PE-2 and PE-3:
 
* Build a character which can toggle between the default FPS weapon, the hit-scan weapon from PE-2, and the Force weapon from PE-3.
	* Build a small UI element which shows what weapon is equipped, and how to toggle between them
	* I would recommend binding the 1/2/3 keys as well as the scroll wheel 
* Make the projectile and “beam” weapons work with the UE Damage System
* Create a new version of the Target, which is Destructible (and can be destroyed by impact)
* Make a level where you can go through, with targets placed around, some targets are behind barriers, and each weapon can be used in different locations to destroy different targets.
	* Note: The Force weapon may struggle to destroy targets, but Impulse should work


### Personnel

In this section, list the primary author of the spike report, as well as any personnel who assisted in completing the work.

* Primary - Jacob Pipers
* Secondary - N/A

### Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.


* [Enums](https://wiki.unrealengine.com/Enums_For_Both_C%2B%2B_and_BP)
* [Making a Destructible](https://docs.unrealengine.com/latest/INT/Videos/PLZlv_N0_O1gYeJX3xX44yzOb7_kTS7FPM/-gGiEmYj4oE/)
* Visual Studio
* Unreal Engine


### Tasks Undertaken

Show only the key tasks that will help the developer/reader understand what is required.

* Combine Code from PE-2 and PE-3
	* Create new c++ FPS project and Create a function for each of the weapons in the Character Class

		* Laser
		* ForcePush
		* Shotgun (Default Gun)

* Changing Weapons - Enums and States

	* Create a enum class for the weapon types as seen below

			UENUM(BlueprintType)
			enum class WeaponTypeEnum : uint8
			{
				None UMETA(DisplayName = "None"),
				Laser UMETA(DisplayName = "Laser"),
				Riffle UMETA(DisplayName = "Riffle"),
				ForcePush UMETA(DisplayName = "Force Push")
			};

	* Create a switch statement in the OnFire function
		* Call Weapon Function based on Current weapon

				switch (CurrentWeapon)
				{
				case WeaponTypeEnum::None:

					break;
				case WeaponTypeEnum::Laser:
					AspikePe4Character::LaserBeam(World);
					break;
				case WeaponTypeEnum::Riffle:
					AspikePe4Character::BulletWeaponFire();
					break;
				case WeaponTypeEnum::ForcePush:
					AspikePe4Character::ForcePush(World);
					break;
				default:
					break;
				}

	* Change Weapon on Key press
		* Bind Acftions to weapon changes as seen below.

				PlayerInputComponent->BindAction("One", IE_Pressed, this, &AspikePe4Character::Weapon1);
				PlayerInputComponent->BindAction("Two", IE_Pressed, this, &AspikePe4Character::Weapon2);
				PlayerInputComponent->BindAction("Three", IE_Pressed, this, &AspikePe4Character::Weapon3);
				PlayerInputComponent->BindAction("WeaponChangeup", IE_Pressed, this, &AspikePe4Character::ChangeWeapon);
				PlayerInputComponent->BindAction("WeaponChangeDown", IE_Pressed, this, &AspikePe4Character::ChangeWeaponDown);


	* Change Weapon with mouse wheel
		* Bind as seen above
		* Use Switch Statement to decide what weapon to change to. Code below ChangeWeapon is On Mouse wheel Scroll up

				switch (CurrentWeapon)
				{
				case WeaponTypeEnum::None:
					ChangeToForcePush();
					break;
				case WeaponTypeEnum::Laser:
					ChangeToNone();
					break;
				case WeaponTypeEnum::Riffle:
					ChangeToLaser();
					break;
				case WeaponTypeEnum::ForcePush:
					ChangeToRiffle();
					break;
				default:
					break;
				}

		* On Mouse Scroll Down do the same but in reverse.

	* Add Weapon UI
		* Create a new widget called Weapon UI
		* Add a Canvas Panel to the designer
		* Add a Image as child to Canvas Panel
		* Add Text as child to Canvas Panel

			![](SpikeImages/UI.png)

	* Bind the Image Appearence Brush
		* Make Slate Brush Get the Current Weapon and change Image ussing swich statement. As Seen In Images Below.
			
			![](SpikeImages/UISwitch.png)

		

* Damage System 

	* Make A Cube Mesh Destructable [Follow this tutorial](https://docs.unrealengine.com/latest/INT/Videos/PLZlv_N0_O1gYeJX3xX44yzOb7_kTS7FPM/-gGiEmYj4oE/)

		* In Destructable Settings Enable Impact Damage.

	* Repeate the step above this time with SM_ROCK Asset. Without enabling Impact Damage.

		* Tick the Flags Accumilate Damage and World Support.

	* Put a combination of Rocks and cubes in the world
		* For the rocks Stimulate Physics 
		* The Cubses add acouple some with stimulated physics and some without.

	* To Make The Beam weapon work with the Damage system Check to see if the hit Object has a UDestructibleComponent attached. If Does [Apply Radius Damage](https://docs.unrealengine.com/latest/INT/API/Runtime/Engine/Components/UDestructibleComponent/ApplyRadiusDamage/index.html).
		
			UDestructibleComponent * ComponentToDamage = Cast<UDestructibleComponent>(FireHit.GetComponent());

			if (ComponentToDamage != NULL)
			{
				float BaseDamage = 100; // TODO: Get rid of magic number.
				AController * EventInstigator = UGameplayStatics::GetPlayerController(World, 0);
				AActor * DamageCauser = this;
				TSubclassOf <class UDamageType> DamageTypeClass;

				ComponentToDamage->ApplyRadiusDamage(BaseDamage, FireHit.ImpactPoint, 100, 100, true);
			}



### What we found out



### [Optional] Open Issues/Risks

The Destructable May Float if World Support is not ticked in the flags Specificalley if object is not already stimulating physics.

List out the issues and risks that you have been unable to resolve at the end of the spike. You may have uncovered a whole range of new risks as well.

### [Optional] Recommendations

You may state that another spike is required to resolve new issues identified (or) indicate that this spike has increased the team�s confidence in XYZ and move on.